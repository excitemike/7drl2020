import * as ROT from "rot-js/lib/index"
import Simple from "rot-js/lib/scheduler/simple"
import { CreateCallback } from "rot-js/lib/map/map"
import * as gamepad from "./gamepad"
import * as mapgen from "./mapgen"
import stamp from "./stamp"
import * as settings from "./settings"
import * as ecs from "./ecs"
import comps from "./comps"
import DistanceMap from "./distancemap"

type Cell = {
  x: number,
  y: number
};

const
  EPSILON = 0.001,
  floor = Math.floor,
  max = Math.max,
  min = Math.min,
  PI = Math.PI,
  random = ROT.RNG.getUniform.bind(ROT.RNG),
  randRange = ROT.RNG.getUniformInt.bind(ROT.RNG),
  randFromArray = ROT.RNG.getItem.bind(ROT.RNG),
  sqrt = Math.sqrt,
  TAU = 2 * PI,

  // 2d arrays to track things per cell
  isWall: number[][] = [],
  blockerCount: number[][] = [],

  // settings
  w = 45,
  h = 25,
  mainDisplayFontSize = 18,
  messageAreaW = 35,
  messageAreaH = 18,
  messageAreaFontSize = 15,
  keyMap: { [key: string]: number } = { "ArrowUp": 0, "Up": 0, "W": 0, "w": 0, "K": 0, "k": 0, "ArrowDown": 4, "Down": 4, "S": 4, "s": 4, "J": 4, "j": 4, "ArrowLeft": 6, "Left": 6, "A": 6, "a": 6, "H": 6, "h": 6, "ArrowRight": 2, "Right": 2, "D": 2, "d": 2, "L": 2, "l": 2 },
  ambushDist = 7,
  maintainRadiusDist = 7,
  initialBombs = 1,

  // ANIMATION SETTINGS
  restartCurtainFallFrames = 30,
  restartCurtainPauseFrames = 15,
  restartCurtainRiseFrames = 15,
  levelChangeOutFrames = 30, // naming here is arguable backwards - this is frames for animating the current level out
  levelChangeInFrames = 45, // frames of animated next level in
  bombInFrames = 3,
  bombOutFrames = 6,

  // COLOR SETTINGS
  tailColorSeq = [
    "#bfb0ff",
    "#00eaff",
    "#00f2c6",
    "#94de00",
    "#ff9e00",
    "#ff58cd"
  ],
  floorColor = "#000",
  wallColor = "#999",
  happyColor = "#ff0",
  bombColorGradient = function(t: number) {
    var red = max(0, min(255, 255 * (3 - 3 * t))),
      green = max(0, min(255, 255 * (2 - 3 * t))),
      blue = max(0, min(255, 255 * (1 - 3 * t)));
    return "rgb(" + red.toFixed() + ", " + green.toFixed() + ", " + blue.toFixed() + ")";
  };

let tailColorSeqI = 0,
  notesInDungeon = 0,
  display: ROT.Display,
  messageDisplay: ROT.Display,
  scheduler: Simple,
  engine: ROT.Engine,

  // stats
  runStatsCurrent = { notes: 0, level: 1 },
  runStatsPrev = { notes: 0, level: 1 },
  runStatsBest = { notes: 0, level: 1 },

  characters: {
    [key: string]: {
      component: ecs.C,
      act: () => void,
      color: string
    }
  },
  characterActorFunctions: (() => void)[];

function shuffle(a: any[]) {
  var n = a.length,
    i,
    index,
    temp;
  for (i = 0; i < n; i += 1) {
    index = floor(random() * (n - 1 - i));
    temp = a[0];
    a[0] = a[index];
    a[index] = temp;
  }
}

function drawStats() {
  var x,
    y;
  for (y = 3; y < 9; y += 1) {
    for (x = 0; x < messageAreaW; x += 1) {
      messageDisplay.draw(x, y, " ", null, null);
    }
  }

  ecs.s1(comps.carriedBombs, function(_: string, nBombs: number) {
    messageDisplay.drawText(0, 3, "Bombs: " + nBombs.toString());
  });
  ecs.s1(comps.notesCollectedThisLevel, function(_: string, nNotes: number) {
    messageDisplay.drawText(0, 4, "♪: " + nNotes.toString() + " / " + (nNotes + notesInDungeon).toString());
  });

  messageDisplay.drawText(0, 6, "Level: " + runStatsCurrent.level.toString());
  messageDisplay.drawText(0, 7, "Last:  " + runStatsPrev.level.toString());
  messageDisplay.drawText(0, 8, "Best:  " + runStatsBest.level.toString());
}

function notesPickup() {
  ecs.s4(comps.player, comps.x, comps.y, comps.notesCollectedThisLevel,
    function(player: string, _: null, playerX: number, playerY: number, notesThisLevel: number): void {
      ecs.s3(comps.note, comps.x, comps.y,
        function(note: string, _: null, noteX: number, noteY: number): void {
          if ((playerX === noteX) && (playerY === noteY)) {
            ecs.deleteE(note);
            notesInDungeon -= 1;
            if (notesInDungeon === 0) {
              showMessage("Level complete!");
              doLevelChange();
            } else if (notesInDungeon == 1) {
              showMessage("Collect last ♪ to go to next level!");
            } else {
              showMessage("♪ collected! " + notesInDungeon + " remain");
            }
            runStatsCurrent.notes += 1;
            comps.notesCollectedThisLevel.set(player, notesThisLevel + 1);
            runStatsBest.notes = max(runStatsCurrent.notes, runStatsBest.notes);
            drawStats();
            addTailPiece(player, "#fff");
          }
        }
      );
    }
  );
}
function itemPickup() {
  notesPickup();

  ecs.s3(comps.bomb, comps.x, comps.y, function(bomb: string, _: null, bombX: number, bombY: number): void {
    ecs.s4(comps.player, comps.x, comps.y, comps.carriedBombs, function(player: string, _: null, playerX: number, playerY: number, bombCount: number): void {
      if ((playerX === bombX) && (playerY === bombY)) {
        comps.carriedBombs.set(player, bombCount + 1);
        drawStats();
        ecs.deleteE(bomb);
      }
    });
  });
}

function playerAct() {
  ecs.s2(comps.player, comps.carriedBombs, function(player: string, _: null, numBombs: number): void {
    if (!canMove(player)) {
      if (numBombs === 0) {
        render();
        showMessage("Trapped! Press R to restart.");
      } else {
        showMessage("Boxed in! Press B to use a bomb and escape!");
      }
    }
    beginPlayerInput();
  });
}
function beginPlayerInput() {
  engine.lock();
  gamepad.addListener(playerInput);
  window.addEventListener("keydown", playerInput);
}
function endPlayerInput() {
  gamepad.removeListener(playerInput);
  window.removeEventListener("keydown", playerInput);
  eraseMessage();
  engine.unlock();
}
function wandererAct() {
  ecs.s3(comps.wanderer, comps.x, comps.y, wander);
}
function seekerAct() {
  ecs.s3(comps.seeker, comps.x, comps.y, function(seeker: string, _: null, seekerX: number, seekerY: number): void {
    let bestPathLen = 1000000,
      bestPath: Cell[] = [];
    ecs.s3(comps.player, comps.x, comps.y, function(_0: string, _1: null, playerX: number, playerY: number): void {
      const passableCb = function(x: number, y: number) {
        return (0 === blockerCount[y][x]) || ((x == seekerX) && (y == seekerY)) || ((x == playerX) && (y == playerY));
      },
        astar = new ROT.Path.AStar(playerX, playerY, passableCb, { topology: 4 }),
        path: Cell[] = [],
        pathCb = (function(path) {
          return function(x: number, y: number) {
            path.push({ x, y });
          };
        }(path));
      astar.compute(seekerX, seekerY, pathCb);
      if ((path.length > 1) && (path.length < bestPathLen)) {
        bestPathLen = path.length;
        bestPath = path;
      }
    });
    if (bestPath.length === 0) {
      wander(seeker, null, seekerX, seekerY);
    } else {
      if (bestPath.length > 2) {
        var dest = bestPath[1];
        moveEntity(seeker, dest.x, dest.y);
      }
    }
  });
}
function ambusherAct() {
  ecs.s3(comps.ambusher, comps.x, comps.y, function(id: string, _: null, ambusherX: number, ambusherY: number): void {
    let bestPathLen = 1000000,
      bestPath: Cell[] = [];
    ecs.s3(comps.player, comps.x, comps.y, function(player: string, _: null, playerX: number, playerY: number): void {
      const behind = comps.followedBy.get(player),
        stepX = playerX - comps.x.get(behind),
        stepY = playerY - comps.y.get(behind),
        targetX = max(1, min(w - 2, playerX + ambushDist * stepX)),
        targetY = max(1, min(h - 2, playerY + ambushDist * stepY)),
        passableCb = function(x: number, y: number) {
          return (0 === blockerCount[y][x]) || ((x === ambusherX) && (y === ambusherY)) || ((x === targetX) && (y === targetY));
        },
        astar = new ROT.Path.AStar(targetX, targetY, passableCb, { topology: 4 }),
        path: Cell[] = [],
        pathCb = (function(path) {
          return function(x: number, y: number) {
            path.push({ x, y });
          };
        }(path));
      astar.compute(ambusherX, ambusherY, pathCb);
      if ((path.length > 1) && (path.length < bestPathLen)) {
        bestPathLen = path.length;
        bestPath = path;
      }
    });
    if (bestPath.length > 2) {
      var dest = bestPath[1];
      moveEntity(id, dest.x, dest.y);
    } else {
      const behind = comps.followedBy.get(id),
        targetX = 2 * ambusherX - comps.x.get(behind),
        targetY = 2 * ambusherY - comps.y.get(behind);
      if (0 === blockerCount[targetY][targetX]) {
        moveEntity(id, targetX, targetY);
      } else {
        wander(id, null, ambusherX, ambusherY);
      }
    }
  });
}
function maintainRadiusAct() {
  ecs.s3(comps.maintainRadius, comps.x, comps.y, function(id: string, _: null, mrX: number, mrY: number): void {
    let bestPathLen = 1000000,
      bestPath: Cell[] = [];
    ecs.s3(comps.player, comps.x, comps.y, function(_0: string, _1: null, playerX: number, playerY: number): void {
      const passableCb = function(x: number, y: number) {
        return (0 === blockerCount[y][x]) || ((x === mrX) && (y === mrY));
      },
        astar = new ROT.Path.AStar(playerX, playerY, passableCb, { topology: 4 }),
        path: Cell[] = [],
        pathCb = function(x: number, y: number) {
          path.push({ x, y });
        };
      astar.compute(mrX, mrY, pathCb);
      if ((path.length > maintainRadiusDist) && (path.length < bestPathLen)) {
        bestPathLen = path.length;
        bestPath = path;
      }
    });
    if (bestPath.length > 2) {
      var dest = bestPath[1];
      moveEntity(id, dest.x, dest.y);
    } else {
      const behind = comps.followedBy.get(id),
        targetX = 2 * mrX - comps.x.get(behind),
        targetY = 2 * mrY - comps.y.get(behind);
      if (0 === blockerCount[targetY][targetX]) {
        moveEntity(id, targetX, targetY);
      } else {
        wander(id, null, mrX, mrY);
      }
    }
  });
}


characters = {
  "@": { component: comps.player, act: playerAct, color: "#fff" },
  "W": { component: comps.wanderer, act: wandererAct, color: "#ff58cd" },
  "S": { component: comps.seeker, act: seekerAct, color: "#ff9e00" },
  "A": { component: comps.ambusher, act: ambusherAct, color: "#94de00" },
  "R": { component: comps.maintainRadius, act: maintainRadiusAct, color: "#00f2c6" }
};
characterActorFunctions = [wandererAct, seekerAct, ambusherAct, maintainRadiusAct];

// animate and change level
function doLevelChange() {
  beginAnimatedLevelChange();
}
// do bomb effects and animation
function doBomb() {
  engine.lock();
  var unsafeBackend = (display._backend as any),
    ctx: CanvasRenderingContext2D = unsafeBackend._ctx,
    spacingX: number = unsafeBackend._spacingX,
    spacingY: number = unsafeBackend._spacingY,
    bombR = settings.bombRadius * max(spacingX, spacingY),
    bombFrame = 0,
    fillBackup = ctx.fillStyle,
    bombIn = function() {
      if (bombFrame < bombInFrames) {
        bombFrame += 1;
        var t = sqrt(bombFrame / bombInFrames),
          r = bombR * t;
        ctx.fillStyle = "#fff";
        ecs.s3(comps.player, comps.x, comps.y, function(_0: string, _1: null, playerX: number, playerY: number): void {
          ctx.beginPath();
          ctx.arc((playerX + 0.5) * spacingX, (playerY + 0.5) * spacingY, r, 0, TAU);
          ctx.fill();
        });
        window.requestAnimationFrame(bombIn);
      } else {
        // bomb effects
        ecs.s3(comps.player, comps.x, comps.y, function(_0: string, _1: null, playerX: number, playerY: number): void {
          const xMin = playerX - floor(bombR / spacingX),
            xMax = playerX + floor(bombR / spacingX),
            yMin = playerY - floor(bombR / spacingY),
            yMax = playerY + floor(bombR / spacingY);

          // remove walls
          for (var y = yMin; y <= yMax; y += 1) {
            for (var x = xMin; x <= xMax; x += 1) {
              var dx = (x - playerX) * spacingX,
                dy = (y - playerY) * spacingY,
                d = sqrt(dx * dx + dy * dy);
              if (d <= bombR + EPSILON) {
                bombCell(x, y);
              }
            }
          }

          // destroy enemies
          Object.keys(characters).forEach(function(symbol) {
            ecs.s4(characters[symbol].component, comps.x, comps.y, comps.symbol, function(id: string, _: null, x: number, y: number, symbol: string): void {
              if (symbol === "@") { return; }
              const dx = (x - playerX) * spacingX,
                dy = (y - playerY) * spacingY,
                d = sqrt(dx * dx + dy * dy);
              if (d <= bombR + EPSILON) {
                deleteCharacter(id);
              }
            });
          });

          // retract tails
          Object.keys(characters).forEach(function(symbol) {
            ecs.s1(characters[symbol].component, function(id: string, _: null): void {
              while (comps.followedBy.has(id)) {
                var next = comps.followedBy.get(id),
                  fromX = comps.x.get(next),
                  fromY = comps.y.get(next),
                  toX = comps.x.get(id),
                  toY = comps.y.get(id);
                if (comps.blocker.has(next)) {
                  blockerCount[fromY][fromX] -= 1;
                  blockerCount[toY][toX] += 1;
                }
                comps.x.set(next, toX);
                comps.y.set(next, toY);
                id = next;
              }
            });
          });
          showMessage("Bomb used!");
          bombOut();
        });
      }
    },
    bombOut = function() {
      if (bombFrame < bombInFrames + bombOutFrames) {
        bombFrame += 1;
        var t = (bombFrame - bombInFrames) / bombOutFrames;
        render();

        ctx.fillStyle = bombColorGradient(t);
        ecs.s3(comps.player, comps.x, comps.y, function(_0: string, _1: null, x: number, y: number): void {
          ctx.beginPath();
          ctx.arc((x + 0.5) * spacingX, (y + 0.5) * spacingY, bombR, 0, TAU);
          ctx.arc((x + 0.5) * spacingX, (y + 0.5) * spacingY, bombR * t, TAU, 0, true);
          ctx.fill();
        });
        window.requestAnimationFrame(bombOut);
      } else {
        ctx.fillStyle = fillBackup;
        engine.unlock();
      }
    };
  bombIn();
}

// control player
function playerInput(e: KeyboardEvent) {
  ecs.s4(comps.player, comps.x, comps.y, comps.carriedBombs, function(player: string, _: null, fromX: number, fromY: number, numBombs: number): void {
    if (e.key in keyMap) {
      var offset = ROT.DIRS[8][keyMap[e.key]],
        toX = fromX + offset[0],
        toY = fromY + offset[1];
      if (0 === blockerCount[toY][toX]) {
        moveEntity(player, toX, toY);
        endPlayerInput();
      } else {
        showMessage("Can not move that way");
      }
    }
    switch (e.key) {
      case "B":
      case "b":
        if (numBombs > 0) {
          comps.carriedBombs.set(player, numBombs - 1);
          doBomb();
          drawStats();
          endPlayerInput();
        } else if (canMove(player)) {
          showMessage("Out of bombs");
        } else {
          showMessage("Out of bombs. Press R to restart.");
        }
        break;
      case "R":
      case "r":
        beginAnimatedRestart();
        endPlayerInput();
    }
  });
}

// detect when the player has nowhere to go
function canMove(id: string) {
  var x = comps.x.get(id),
    y = comps.y.get(id),
    upBlocked = (y <= 0) || (0 < blockerCount[y - 1][x]),
    downBlocked = (y >= h - 1) || (0 < blockerCount[y + 1][x]),
    leftBlocked = (x <= 0) || (0 < blockerCount[y][x - 1]),
    rightBlocked = (x >= w - 1) || (0 < blockerCount[y][x + 1]);
  return !(upBlocked && downBlocked && leftBlocked && rightBlocked);
}

// remove a character
function deleteCharacter(id: string) {
  const x = comps.x.get(id),
    y = comps.y.get(id);
  if (comps.blocker.has(id)) {
    comps.blocker.delete(id);
    blockerCount[y][x] -= 1;
  }
  {
    let followed = id;
    while (comps.followedBy.has(followed)) {
      const follower = comps.followedBy.get(followed);
      if (comps.blocker.has(follower)) {
        comps.blocker.delete(follower);
        blockerCount[comps.y.get(follower)][comps.x.get(follower)] -= 1;
      }

      ecs.deleteE(followed);
      followed = follower;
    }
    ecs.deleteE(followed);
  }
}

// a bomb hit this cell!
function bombCell(x: number, y: number) {
  if ((y > 0) && (y < h - 1) && (x > 0) && (x < w - 1)) {
    if (isWall[y][x]) {
      isWall[y][x] = 0;
      blockerCount[y][x] -= 1;
    }
  }
}

// move an entity, cascading to its followers
function moveEntity(entity: ecs.E, toX: number, toY: number) {
  var fromX = comps.x.get(entity),
    fromY = comps.y.get(entity);
  comps.x.set(entity, toX);
  comps.y.set(entity, toY);
  if ((toX != fromX) || (toY != fromY)) {
    if (comps.blocker.has(entity)) {
      blockerCount[fromY][fromX] -= 1;
      blockerCount[toY][toX] += 1;
    }
    if (comps.followedBy.has(entity)) {
      updateFollower(entity, fromX, fromY);
    }
  }
}

// cascade entity movement to its followers
function updateFollower(target: string, fromX: number, fromY: number) {
  var follower = comps.followedBy.get(target);
  moveEntity(follower, fromX, fromY);
}

function showMessage(msg: string) {
  eraseMessage();
  messageDisplay.drawText(0, 0, msg, messageAreaW);
}
function showPersistentMessage(s: string) {
  for (var y = 10; y < messageAreaH; y += 1) {
    for (var x = 0; x < messageAreaW; x += 1) {
      messageDisplay.draw(x, y, " ", null, null);
    }
  }
  messageDisplay.drawText(0, 10, s, messageAreaW);
}
function eraseMessage() {
  for (var y = 0; y < 2; y += 1) {
    for (var x = 0; x < messageAreaW; x += 1) {
      messageDisplay.draw(x, y, " ", null, null);
    }
  }
}
function resetScheduler() {
  scheduler._queue.clear();
  scheduler._repeat.forEach(function(item) { scheduler._queue.add(item, 0); });
  scheduler._current = null;
}

// INIT
function init() {
  display = new ROT.Display({
    width: w,
    height: h,
    fg: "#fff",
    bg: "#000",
    forceSquareRatio: false,
    fontSize: mainDisplayFontSize
  });
  let gameview = document.getElementById("gameview");
  if (!!gameview) {
    let container = display.getContainer();
    if (!!container) {
      gameview.appendChild(container);
    }
  }
  messageDisplay = new ROT.Display({
    width: messageAreaW,
    height: messageAreaH,
    fg: floorColor,
    bg: wallColor,
    forceSquareRatio: false,
    fontSize: messageAreaFontSize
  });
  let messagearea = document.getElementById("messagearea");
  if (!!messagearea) {
    let container = messageDisplay.getContainer();
    if (!!container) {
      messagearea.appendChild(container);
    }
  }
  scheduler = new ROT.Scheduler.Simple();

  scheduler.add({ act: render }, true);
  scheduler.add({ act: playerAct }, true);
  scheduler.add({ act: itemPickup }, true);

  characterActorFunctions.forEach(function(f) {
    scheduler.add({ act: f }, true);
  });

  engine = new ROT.Engine(scheduler);
  reset();
  generateMap();

  engine.start();
}
function clearLevel() {
  const clearFn = function(id: string): void {
    ecs.deleteE(id);
  };
  for (var symbol in characters) {
    if (!characters.hasOwnProperty(symbol)) { continue; }
    if (symbol === "@") { continue; }
    ecs.s1(characters[symbol].component, clearFn);
  }

  ecs.s1(comps.deleteOnLevelChange, clearFn);
  ecs.resetComponent(comps.followedBy);
  ecs.resetComponent(comps.follows);
  ecs.resetComponent(comps.notesCollectedThisLevel);

  // vars
  notesInDungeon = 0;

  // clear/populate grids
  for (var y = 0; y < h; y += 1) {
    isWall[y] = [];
    blockerCount[y] = [];
    for (var x = 0; x < w; x += 1) {
      isWall[y][x] = 1;
      blockerCount[y][x] = 0;
    }
  }
}
function reset() {
  clearLevel();

  ecs.clearEntities();

  runStatsPrev.notes = runStatsCurrent.notes;
  runStatsCurrent.notes = 0;
  runStatsPrev.level = runStatsCurrent.level;
  runStatsCurrent.level = 1;
  drawStats();
  resetScheduler();
}

function generateMap() {
  const freeCells = [],
    digCallback: CreateCallback = function(x: number, y: number, value: number) {
      var prevVal = isWall[y][x];
      if (prevVal !== value) {
        if (!value) {
          isWall[y][x] = 0;
          blockerCount[y][x] = 0;
        } else {
          isWall[y][x] = 1;
          blockerCount[y][x] = 1;
        }
      }
    };
  mapgen.generateBaseMap(w, h, runStatsCurrent.level, digCallback);

  stampDeadEnds(digCallback);
  fixDeadEnds(digCallback);
  fixCorners(digCallback);

  for (var y = 0; y < h; y += 1) {
    for (var x = 0; x < w; x += 1) {
      if (!isWall[y][x]) {
        freeCells.push({ x: x, y: y });
      } else {
        blockerCount[y][x] = 1;
      }
    }
  }
  shuffle(freeCells);
  placeThings(freeCells);
  drawStats();

  if (runStatsCurrent.level === 1) {
    showPersistentMessage("You are \"@\"\n\nCollect all \"♪\" to go to next level\nDon't get trapped!\n\nArrow keys or WASD to move\nR key to restart from level 1");
  } else if (runStatsCurrent.level === 2) {
    showPersistentMessage("Collect \"!\" to get bombs\nPress \"b\" key to use them");
  } else {
    showPersistentMessage("");
  }
}
// search map for ugly corners and fix
function fixCorners(digCallback: CreateCallback) {;
  for (let y = 0; y < h-1; y += 1) {
    for (let x = 0; x < w-1; x += 1) {
      if (!isWall[y][x] &&
          isWall[y][x+1] &&
          isWall[y+1][x] &&
          !isWall[y+1][x+1]) {
        if (random() < 0.5) {
          digCallback(x+1, y, 0);
        } else {
          digCallback(x, y+1, 0);
        }
      } else if (isWall[y][x] &&
          !isWall[y][x+1] &&
          !isWall[y+1][x] &&
          isWall[y+1][x+1]) {
        if (random() < 0.5) {
          digCallback(x, y, 0);
        } else {
          digCallback(x+1, y+1, 0);
        }
      }
    }
  }
}
// search map for dead ends and hit them with a stamp
function stampDeadEnds(digCallback: CreateCallback) {
  const freeCells = [];
  let i=0,
    x=0,
    y=0,
    cell,
    upBlocked=false,
    downBlocked=false,
    leftBlocked=false,
    rightBlocked=false,
    sidesBlocked=0;
  for (let y = 0; y < h; y += 1) {
    for (let x = 0; x < w; x += 1) {
      if (!isWall[y][x]) {
        freeCells.push({ x: x, y: y });
      }
    }
  }
  shuffle(freeCells);
  while (i < freeCells.length) {
    cell = freeCells[i];
    i
    x = cell.x;
    y = cell.y;
    upBlocked = (y <= 0) || (isWall[y - 1][x] !== 0);
    downBlocked = (y >= h - 1) || (isWall[y + 1][x] !== 0);
    leftBlocked = (x <= 0) || (isWall[y][x - 1] !== 0);
    rightBlocked = (x >= w - 1) || (isWall[y][x + 1] !== 0);
    sidesBlocked = (upBlocked ? 1 : 0) + (downBlocked ? 1 : 0) + (leftBlocked ? 1 : 0) + (rightBlocked ? 1 : 0);
    if (sidesBlocked >= 3) {
      stamp.applyRandomStampAt(x, y, 1, 1, w - 2, h - 2, digCallback)
    }
    i += 1;
  }
}
// search map for dead ends and connect them to something
function fixDeadEnds(digCallback: CreateCallback) {
  const UP = "up",
    RIGHT = "right",
    DOWN = "down",
    LEFT = "left",
    NONE = "none";
  var i = 0,
    freeCells = [],
    cell,
    x,
    y,
    upBlocked,
    downBlocked,
    leftBlocked,
    rightBlocked,
    sidesBlocked;
  for (y = 0; y < h; y += 1) {
    for (x = 0; x < w; x += 1) {
      if (!isWall[y][x]) {
        freeCells.push({ x: x, y: y });
      }
    }
  }
  shuffle(freeCells);
  while (i < freeCells.length) {
    cell = freeCells[i];
    x = cell.x;
    y = cell.y;
    upBlocked = (y <= 0) || (isWall[y - 1][x]);
    downBlocked = (y >= h - 1) || (isWall[y + 1][x]);
    leftBlocked = (x <= 0) || (isWall[y][x - 1]);
    rightBlocked = (x >= w - 1) || (isWall[y][x + 1]);
    sidesBlocked = (upBlocked ? 1 : 0) + (downBlocked ? 1 : 0) + (leftBlocked ? 1 : 0) + (rightBlocked ? 1 : 0);
    if (sidesBlocked >= 3) {
      const options: { [key: string]: number } = {};
      // try digging up
      if (upBlocked) {
        for (let y2 = y - 1; y2 > 0; y2 -= 1) {
          if (!isWall[y2][x]) {
            options[UP] = w - (y - y2);
            break;
          }
        }
      }
      // try digging down
      if (downBlocked) {
        for (let y2 = y + 1; y2 < h - 1; y2 += 1) {
          if (!isWall[y2][x]) {
            options[DOWN] = w - (y2 - y);
            break;
          }
        }
      }
      // try digging left
      if (leftBlocked) {
        for (let x2 = x - 1; x2 > 0; x2 -= 1) {
          if (!isWall[y][x2]) {
            options[LEFT] = w - (x - x2);
            break;
          }
        }
      }
      // try digging right
      if (rightBlocked) {
        for (let x2 = x + 1; x2 < w - 1; x2 += 1) {
          if (!isWall[y][x2]) {
            options[RIGHT] = w - (x2 - x);
            break;
          }
        }
      }
      // and some chance to just close it up
      options[NONE] = 3 * w;

      const direction = ROT.RNG.getWeightedValue(options);

      if (direction === NONE) {
        // close it off
        digCallback(x, y, 1);
        freeCells.splice(i, 1);
        i = 0;
      } else if (direction === UP) {
        for (let y2 = y - 1; y2 > 1; y2 -= 1) {
          if (isWall[y2][x]) {
            digCallback(x, y2, 0);
          } else {
            i += 1;
            break;
          }
        }
      } else if (direction === DOWN) {
        for (let y2 = y + 1; y2 < h - 1; y2 += 1) {
          if (isWall[y2][x]) {
            digCallback(x, y2, 0);
          } else {
            i += 1;
            break;
          }
        }
      } else if (direction === LEFT) {
        for (let x2 = x - 1; x2 > 1; x2 -= 1) {
          if (isWall[y][x2]) {
            digCallback(x2, y, 0);
          } else {
            i += 1;
            break;
          }
        }
      } else if (direction === RIGHT) {
        for (let x2 = x + 1; x2 < w - 1; x2 += 1) {
          if (isWall[y][x2]) {
            digCallback(x2, y, 0);
          } else {
            i += 1;
            break;
          }
        }
      } else {
        throw new Error();
      }
    } else {
      i += 1;
    }
  }
}
function addCharacter(symbol: string, x: number, y: number, tailLength: number) {
  const data = characters[symbol],
    id = ecs.e();
  data.component.set(id, null);
  comps.blocker.set(id, null);
  if (symbol !== "@") {
    comps.deleteOnLevelChange.set(id, null);
  }
  comps.x.set(id, x);
  comps.y.set(id, y);
  blockerCount[y][x] += 1;
  comps.symbol.set(id, symbol);
  var tailPieces = 0;
  while (tailPieces < tailLength) { addTailPiece(id, data.color); tailPieces += 1; }
  return id;
}

// helper for placeThings
function spacedPlacementBegin(seedX: number, seedY: number): DistanceMap {
  const distanceMap = new DistanceMap(function(x: number, y: number): boolean {
    return ((y > 0) && (y < h - 1) && (x > 0) && (x < w - 1) && (blockerCount[y][x] === 0));
  }, 4);
  distanceMap.addGoal(seedX, seedY);
  return distanceMap;
}
// helper for placeThings
function spacedPlacementNext(distanceMap: DistanceMap, fn: (x: number, y: number) => void): boolean {
  const cell = randFromArray(distanceMap.findFarthest());
  if (!!cell) {
    fn(cell.x, cell.y);
    distanceMap.addGoal(cell.x, cell.y);
    return true;
  }
  return false;
}

// place characters and items in the level
function placeThings(freeCells: Cell[]) {
  if (freeCells.length === 0) { return; }

  var level = runStatsCurrent.level,
    tailLength = floor(settings.tailLengthFn(level)),
    numNotes = floor(randRange(settings.numNotesMinFn(level), settings.numNotesMaxFn(level))),
    numEnemies = floor(randRange(settings.numEnemiesMinFn(level), settings.numEnemiesMaxFn(level))),
    spawnWeights = {
      W: settings.enemyWeightW(level),
      S: settings.enemyWeightS(level),
      A: settings.enemyWeightA(level),
      R: settings.enemyWeightR(level)
    };

  const seedCell = randFromArray(freeCells) || { x: 1, y: 1 },
    distanceMap = spacedPlacementBegin(seedCell.x, seedCell.y);

  // player
  spacedPlacementNext(distanceMap, (x, y) => {
    if (ecs.isEmpty(comps.player)) {
      const player = addCharacter("@", x, y, tailLength);
      if (level === 1) { comps.carriedBombs.set(player, initialBombs); }
      comps.notesCollectedThisLevel.set(player, 0);
    } else {
      ecs.s1(comps.player, function(id: string) {
        comps.x.set(id, x);
        comps.y.set(id, y);
        comps.blocker.set(id, null);
        blockerCount[y][x] += 1;
        comps.notesCollectedThisLevel.set(id, 0);
        if (level === 1) { comps.carriedBombs.set(id, initialBombs); }
        let tailPieces = 0;
        while (tailPieces < tailLength) { addTailPiece(id, characters["@"].color); tailPieces += 1; }
      });

    }
  });

  // collectibles
  let bombsToSpawn = randRange(settings.numBombsMinFn(level), settings.numBombsMaxFn(level));
  while (bombsToSpawn-- > 0) {
    spacedPlacementNext(distanceMap, (x, y) => {

      const id = ecs.e();
      comps.bomb.set(id, null);
      comps.deleteOnLevelChange.set(id, null);
      comps.x.set(id, x);
      comps.y.set(id, y);
    });
  }

  // enemies
  let numEnemiesAdded = 0;
  while (numEnemiesAdded++ < numEnemies) {
    const symbol = ROT.RNG.getWeightedValue(spawnWeights);
    if (undefined !== symbol) {
      spacedPlacementNext(distanceMap, (x, y) => {
        addCharacter(symbol, x, y, tailLength);
      });
    }
  }

  while (notesInDungeon < numNotes) {
    spacedPlacementNext(distanceMap, (x, y) => {
      const id = ecs.e();
      comps.note.set(id, null);
      comps.deleteOnLevelChange.set(id, null);
      comps.x.set(id, x);
      comps.y.set(id, y);
      notesInDungeon += 1;
    });
  }
}
function addTailPiece(head: ecs.E, color: string) {
  const tailPiece = ecs.e();
  comps.tailPieces.set(tailPiece, null);
  comps.blocker.set(tailPiece, null);
  comps.deleteOnLevelChange.set(tailPiece, null);
  var x = comps.x.get(head);
  var y = comps.y.get(head);
  blockerCount[y][x] += 1;
  comps.x.set(tailPiece, x);
  comps.y.set(tailPiece, y);

  if (comps.player.has(head)) {
    comps.tailColor.set(tailPiece, tailColorSeq[tailColorSeqI]);
    tailColorSeqI = (tailColorSeqI + 1) % tailColorSeq.length;
  } else {
    comps.tailColor.set(tailPiece, color);
  }
  comps.follows.set(tailPiece, head);
  if (comps.followedBy.has(head)) {
    const oldTail = comps.followedBy.get(head);
    comps.follows.set(oldTail, tailPiece);
    comps.followedBy.set(tailPiece, oldTail);
  }
  comps.followedBy.set(head, tailPiece);
}
function getTailCharacter(tailPiece: string) {
  var target = comps.follows.get(tailPiece),
    dx = comps.x.get(target) - comps.x.get(tailPiece),
    dy = comps.y.get(target) - comps.y.get(tailPiece),
    headUp = dy < 0,
    headDown = dy > 0,
    headLeft = dx < 0,
    headRight = dx > 0;
  if (comps.followedBy.has(tailPiece)) {
    const follower = comps.followedBy.get(tailPiece),
      dx2 = comps.x.get(follower) - comps.x.get(tailPiece),
      dy2 = comps.y.get(follower) - comps.y.get(tailPiece),
      tailUp = dy2 < 0,
      tailDown = dy2 > 0,
      tailLeft = dx2 < 0,
      tailRight = dx2 > 0;
    if (headUp) {
      if (tailDown) { return /*"│"*/"║"; }
      if (tailLeft) { return /*"╯"*/"╝"; }
      if (tailRight) { return /*"╰"*/"╚"; }
    } else if (headDown) {
      if (tailUp) { return /*"│"*/"║"; }
      if (tailLeft) { return /*"╮"*/"╗"; }
      if (tailRight) { return /*"╭"*/"╔"; }
    } else if (headLeft) {
      if (tailUp) { return /*"╯"*/"╝"; }
      if (tailDown) { return /*"╮"*/"╗"; }
      if (tailRight) { return /*"━"*/"═"; }
    } else if (headRight) {
      if (tailUp) { return /*"╰"*/"╚"; }
      if (tailDown) { return /*"╭"*/"╔"; }
      if (tailLeft) { return /*"━"*/"═"; }
    }
  }
  if (dx > 0) {
    return /*"╶"*/"═";
  } else if (dx < 0) {
    return /*"╸"*/"═";
  } else if (dy > 0) {
    return /*"╻"*/"║";
  } else if (dy < 0) {
    return /*"╵"*/"║";
  }
  return "•";//"○";
}
// render a character
function renderCharacter(id: string) {
  var symbol = comps.symbol.get(id),
    color = characters[symbol].color;
  display.draw(comps.x.get(id), comps.y.get(id), symbol, color, null);
}
function render() {
  for (var y = 0; y < h; y += 1) {
    for (var x = 0; x < w; x += 1) {
      if (isWall[y][x]) {
        display.draw(x, y, " ", floorColor, wallColor);
      } else {
        display.draw(x, y, " ", wallColor, floorColor);
      }
    }
  }
  ecs.s3(comps.note, comps.x, comps.y, function(_0: string, _1: null, x: number, y: number) {
    display.draw(x, y, (notesInDungeon === 1) ? "♫" : "♪", happyColor, null);
  });
  ecs.s3(comps.bomb, comps.x, comps.y, function(_0: string, _1: null, x: number, y: number) {
    display.draw(x, y, "!", happyColor, null);
  });
  ecs.s4(comps.tailPieces, comps.x, comps.y, comps.tailColor, function(id: string, _: null, x: number, y: number, tailColor: string) {
    display.draw(x, y, getTailCharacter(id), tailColor, null);
  });

  for (var symbol in characters) {
    if (characters.hasOwnProperty(symbol)) {
      var data = characters[symbol];
      ecs.s1(data.component, function(id: string) { renderCharacter(id); });
    }
  }
}
// wanderer behavior
function wander(id: string, _: null, x: number, y: number): void {
  const options = [];
  if (0 === blockerCount[y - 1][x]) {
    options.push([x, y - 1]);
  }
  if (0 === blockerCount[y + 1][x]) {
    options.push([x, y + 1]);
  }
  if (0 === blockerCount[y][x - 1]) {
    options.push([x - 1, y]);
  }
  if (0 === blockerCount[y][x + 1]) {
    options.push([x + 1, y]);
  }
  var choice;
  if (options.length > 0) {
    choice = options[floor(random() * options.length)];
    moveEntity(id, choice[0], choice[1]);
    return;
  }

  // if you can't move, pull your tail in
  var piece = id;
  while (comps.followedBy.has(piece)) {
    piece = comps.followedBy.get(piece);
    if ((comps.x.get(piece) != x) || (comps.y.get(piece) != y)) {
      moveEntity(piece, x, y);
      return;
    }
  }

  // tail is pulled in, still stuck. just wait I guess?
}

// generate next level
function nextLevel() {
  clearLevel();
  runStatsCurrent.level += 1;
  runStatsBest.level = max(runStatsCurrent.level, runStatsBest.level);
  generateMap();
}

// animate the level going out
function beginAnimatedLevelChange() {
  engine.lock();
  let outFrame = 0,
    inFrame = 0;
  const unsafeBackend = (display._backend as any),
    ctx: CanvasRenderingContext2D = unsafeBackend._ctx,
    spacingX: number = unsafeBackend._spacingX,
    spacingY: number = unsafeBackend._spacingY,
    fillBackup = ctx.fillStyle,
    circleMargin = 0.5 * spacingX,
    outFn = function() {
      var t: number;
      if (outFrame < levelChangeOutFrames) {
        outFrame += 1;
        t = outFrame / levelChangeOutFrames;
        ecs.s3(comps.player, comps.x, comps.y, function(_0: string, _1: null, playerX: number, playerY: number) {
          var x = playerX + 0.5,
            y = playerY + 0.5,
            rx = max(x, w - x) * spacingX,
            ry = max(y, h - y) * spacingY,
            maxR = sqrt(rx * rx + ry * ry) + circleMargin;
          ctx.fillStyle = happyColor;
          ctx.beginPath();
          ctx.arc(x * spacingX, y * spacingY, t * maxR, 0, TAU);
          ctx.fill();
          ctx.fillStyle = settings.levelChangeColor;
          ctx.beginPath();
          ctx.arc(x * spacingX, y * spacingY, t * maxR - circleMargin, 0, TAU);
          ctx.fill();
        });
        window.requestAnimationFrame(outFn);
      } else {
        nextLevel();
        inFn();
      }
    },
    inFn = function() {
      var t: number;
      if (inFrame < levelChangeInFrames) {
        inFrame += 1;
        t = inFrame / levelChangeInFrames;
        render();
        ecs.s3(comps.player, comps.x, comps.y, function(_0: string, _1: null, playerX: number, playerY: number): void {
          var x = playerX + 0.5,
            y = playerY + 0.5,
            rx = max(x, w - x) * spacingX,
            ry = max(y, h - y) * spacingY,
            maxR = sqrt(rx * rx + ry * ry) + circleMargin;
          ctx.fillStyle = happyColor;
          ctx.beginPath();
          ctx.arc(x * spacingX, y * spacingY, maxR, 0, TAU);
          ctx.arc(x * spacingX, y * spacingY, max(0, t * maxR - circleMargin), 0, TAU, true);
          ctx.fill();
          ctx.fillStyle = settings.levelChangeColor;
          ctx.beginPath();
          ctx.arc(x * spacingX, y * spacingY, maxR, 0, TAU);
          ctx.arc(x * spacingX, y * spacingY, t * maxR, 0, TAU, true);
          ctx.fill();
        });
        window.requestAnimationFrame(inFn);
      } else {
        ctx.fillStyle = fillBackup;
        showMessage("Level " + runStatsCurrent.level + " begin!");
        resetScheduler();
        engine.unlock();
      }
    };
  outFn();
}

// custom ease function
function curtainBounceEase(t: number) {
  var t1 = 0.67,
    t2 = 1.0;
  if (t < t1) {
    return t * t / (t1 * t1);
  } else if (t < t2) {
    return (t * t - (t1 + t2) * (t - t1)) / (t1 * t1);
  }
  return 1;
}
// animate the restart going out
function beginAnimatedRestart() {
  engine.lock();
  var unsafeBackend = (display._backend as any),
    ctx: CanvasRenderingContext2D = unsafeBackend._ctx,
    spacingX: number = unsafeBackend._spacingX,
    spacingY: number = unsafeBackend._spacingY,
    fillBackup = ctx.fillStyle,
    fallFrame = 0,
    pauseFrame = 0,
    riseFrame = 0,
    fallFn = function() {
      var t,
        y;
      if (fallFrame < restartCurtainFallFrames) {
        fallFrame += 1;
        t = fallFrame / restartCurtainFallFrames;
        y = 0.25 + (h - 1) * curtainBounceEase(t);
        render();
        ctx.fillStyle = settings.curtainColor;
        ctx.fillRect(0, 0, w * spacingX, y * spacingY);
        window.requestAnimationFrame(fallFn);
      } else {
        reset();
        generateMap();
        pauseFn();
      }
    },
    pauseFn = function() {
      pauseFrame += 1;
      if (pauseFrame < restartCurtainPauseFrames) {
        ctx.fillStyle = settings.curtainColor;
        ctx.fillRect(0, 0, w * spacingX, h * spacingY);
        window.requestAnimationFrame(pauseFn);
      } else {
        riseFn();
      }
    },
    riseFn = function() {
      var t,
        y;
      if (riseFrame < restartCurtainRiseFrames) {
        riseFrame += 1;
        t = riseFrame / restartCurtainRiseFrames,
          y = h * (1 - t) * (1 - t);
        render();
        ctx.fillStyle = settings.curtainColor;
        ctx.fillRect(0, 0, w * spacingX, y * spacingY);
        window.requestAnimationFrame(riseFn);
      } else {
        ctx.fillStyle = fillBackup;
        engine.unlock();
      }
    };
  fallFn();
}

// wait for dom to load
if (document.readyState === "loading") {
  document.addEventListener("DOMContentLoaded", init);
} else {
  // `DOMContentLoaded` already fired
  init();
}

export default {};
