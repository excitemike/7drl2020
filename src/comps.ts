import { c } from "./ecs"

const comps = {
  player: c(),
  wanderer: c(),
  seeker: c(),
  ambusher: c(),
  maintainRadius: c(),
  tailPieces: c(),
  blocker: c(),
  tailColor: c(),
  x: c(),
  y: c(),
  follows: c(),
  followedBy: c(),
  note: c(),
  carriedBombs: c(),
  bomb: c(),
  symbol: c(),
  notesCollectedThisLevel: c(),
  deleteOnLevelChange: c()
};

export default comps;
