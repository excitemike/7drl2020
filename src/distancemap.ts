// distanceMap.ts
import { PassableCallback } from "rot-js/lib/path/path";

export const NONE = "none";
export const UP = "up";
export const DOWN = "down";
export const LEFT = "left";
export const RIGHT = "right";
export type Direction = "none" | "up" | "down" | "left" | "right";
export type Cell = {
  x: number,
  y: number
};

// maintain distance information across a 2d grid of cells
export default class DistanceMap {
  // distances at each cell
  _data: number[][] = [];

  // cached directions in order
  _dirs: [number, number][];

  // cached callback used to determine whether a cell is passable
  _passableCb: PassableCallback;

  // create
  constructor(passableCallback: PassableCallback, topology: 4 | 6 | 8 | undefined) {
    if (topology === undefined) { topology = 8; }
    this._passableCb = passableCallback;

    // reordered to favor prettier paths
    this._dirs =
      (topology === 4) ? [[-1, 0], [1, 0], [0, -1], [0, 1]] :
        (topology === 8) ? [[-1, 0], [1, 0], [0, -1], [0, 1], [1, -1], [1, 1], [-1, 1], [-1, -1]] :
          [[-2, 0], [2, 0], [-1, -1], [1, -1], [1, 1], [-1, 1],];
  }

  // get the neighbors from given cell
  _getNeighbors(cx: number, cy: number): [number, number][] {
    const result: [number, number][] = [],
      ndirs = this._dirs.length;
    for (let i = 0; i < ndirs; i++) {
      const dir = this._dirs[i];
      let x = cx + dir[0];
      let y = cy + dir[1];
      if (!this._passableCb(x, y)) { continue; }
      result.push([x, y]);
    }
    return result;
  }

  // update cell values
  _update(x: number, y: number, value: number): void {
    const stack: [number, number, number][] = [[x, y, value]],
      data = this._data;

    while (stack.length !== 0) {
      const xyv = stack.shift() || [0, 0, 0],
        x = xyv[0],
        y = xyv[1],
        value = xyv[2];
      if (!(y in data)) {
        data[y] = [];
      }
      const row = data[y];
      if (!!row && (x in row) && (row[x] <= value)) {
        continue;
      }
      if (!(x in row) || (row[x] > value)) {
        row[x] = value;
        const neighbors = this._getNeighbors(x, y),
          numNeighbors = neighbors.length,
          nextValue = value + 1;
        for (let i = 0; i < numNeighbors; i += 1) {
          const neighbor = neighbors[i];
          stack.push([neighbor[0], neighbor[1], nextValue]);
        }
      }
    }
  }

  // consider the given cell a goal cell
  addGoal(x: number, y: number): void {
    this._update(x, y, 0);
  }

  // check a cell's distance from the nearest goal cell
  // (returns Number.POSITIVE_INFINITY if it can't reach a goal cell)
  getDistance(x: number, y: number): number {
    const data = this._data;
    if (y in data) {
      const row = data[y];
      if (x in row) {
        return row[x];
      }
    }
    return Number.POSITIVE_INFINITY;
  }

  // build and return an array of cells that are farthest from any goal,
  // but can still reach it
  // may be empty!
  findFarthest(): Cell[] {
    const data = this._data,
      farthestCells = [];
    let farthestDistance = -1;
    for (const [y, row] of data.entries()) {
      if (!!row) {
        for (const [x, distance] of row.entries()) {
          if (distance === farthestDistance) {
            farthestCells.push({ x, y });
          } else if (distance > farthestDistance) {
            farthestDistance = distance;
            farthestCells.length = 0;
            farthestCells.push({ x, y });
          }
        }
      }
    }
    return farthestCells;
  }
}
