// functions for wrangling game data and behavior into entities, components, and systems

let counter = 0;

// entity
export type E = string;

type CData = { [key: string]: any };

// component getter, setter, and id
export type CId = string;
export class C {
  readonly _data: CData;
  readonly _id: CId;

  constructor(data: CData, id: CId) { this._data = data; this._id = id; }
  get(id: E) { return this._data[id]; }
  set(id: E, value: any) { this._data[id] = value; }
  delete(id: E) { delete this._data[id]; }
  has(id: E) { return (id in this._data); }
  entities(): E[] { return Object.keys(this._data); }
};

const allEs: { [key: string]: E } = {};
const allCs: { [key: string]: C } = {};
const freeIds: string[] = [];

// system
export type S1Callback = (e: E, value: any) => void;
export type S2Callback = (e: E, value0: any, value1: any) => void;
export type S3Callback = (e: E, value0: any, value1: any, value2: any) => void;
export type S4Callback = (e: E, value0: any, value1: any, value2: any, value3: any) => void;
export type S5Callback = (e: E, value0: any, value1: any, value2: any, value3: any, value4: any) => void;

function takeId(): string {
  if (freeIds.length > 0) {
    return freeIds.pop() || "";
  }
  if (counter >= Number.MAX_SAFE_INTEGER) {
    throw new Error("ran out of ids");
  }
  return "`" + (counter++).toString(36);
}

// create an entity
export function e(): E {
  const id = takeId();
  allEs[id] = id;
  return id;
}

// create a component
export function c(): C {
  const id = takeId(),
    component = new C({}, id);
  allCs[id] = component;
  return component;
}

// run some code on components
export function s1(c0: C, fn: S1Callback): void {
  const entityList = c0.entities();
  for (let ei = 0; ei < entityList.length; ei += 1) {
    const e = entityList[ei];
    fn(e, c0.get(e));
  }
}

// run some code on components
export function s2(c0: C, c1: C, fn: S2Callback): void {
  const entityList = c0.entities();
  for (let ei = 0; ei < entityList.length; ei += 1) {
    const e = entityList[ei];
    if (c1.has(e)) {
      fn(e, c0.get(e), c1.get(e));
    }
  }
}

// run some code on components
export function s3(c0: C, c1: C, c2: C, fn: S3Callback): void {
  const entityList = c0.entities();
  for (let ei = 0; ei < entityList.length; ei += 1) {
    const e = entityList[ei];
    if (c1.has(e) && c2.has(e)) {
      fn(e, c0.get(e), c1.get(e), c2.get(e));
    }
  }
}

// run some code on components
export function s4(c0: C, c1: C, c2: C, c3: C, fn: S4Callback): void {
  const entityList = c0.entities();
  for (let ei = 0; ei < entityList.length; ei += 1) {
    const e = entityList[ei];
    if (c1.has(e) && c2.has(e) && c3.has(e)) {
      fn(e, c0.get(e), c1.get(e), c2.get(e), c3.get(e));
    }
  }
}

// run some code on components
export function s5(c0: C, c1: C, c2: C, c3: C, c4: C, fn: S5Callback): void {
  const entityList = c0.entities();
  for (let ei = 0; ei < entityList.length; ei += 1) {
    const e = entityList[ei];
    if (c1.has(e) && c2.has(e) && c3.has(e) && c4.has(e)) {
      fn(e, c0.get(e), c1.get(e), c2.get(e), c3.get(e), c4.get(e));
    }
  }
}

// remove an entity
export function deleteE(e: E): void {
  Object.keys(allCs).forEach(function(cId: string): void {
    allCs[cId].delete(e);
  });
  delete allEs[e];
  if (-1 === freeIds.indexOf(e)) {
    freeIds.push(e);
  }
}

// remove all keys from object
function clearObject(o: { [key: string]: any }): void {
  Object.keys(o).forEach(function(key: string) { delete o[key]; });
}


function deleteCById(id: string): void {
  if (id in allCs) {
    const component = allCs[id];
    clearObject(component._data);
    delete allCs[id];
    freeIds.push(id);
  }
}

// remove a component
export function deleteC(c: C): void {
  deleteCById(c._id);
}

// remove this component's data from ALL entities
export function resetComponent(c: C): void {
  const id = c._id;
  if (id in allCs) {
    const component = allCs[id];
    clearObject(component._data);
  }
}

// check whether any instances of component exist
export function isEmpty(c: C): boolean {
  for (let key in c._data) {
    if (c._data.hasOwnProperty(key)) {
      return false;
    }
  }
  return true;
}

// clear all entities, but leave components
export function clearEntities() {
  Object.keys(allEs).forEach(deleteE);
}

// clear everything - removes all entities and components
export function reset() {
  counter = 0;
  Object.keys(allEs).forEach(deleteE);
  Object.keys(allCs).forEach(deleteCById);
  freeIds.length = 0;
}
