// wrangle game data

// remove all keys from object
function clearObject(o: { [key: string]: any }): void {
  for (let key in o) {
    if (o.hasOwnProperty(key)) {
      delete o[key];
    }
  }
}

// array of items in a1 but not in a2
function arrayExcept(a1: any[], a2: any[]): any[] {
  const result: any[] = [],
    a1len = a1.length;
  for (let i = 0; i < a1len; i += 1) {
    const v = a1[i];
    if (-1 === a2.indexOf(v)) {
      result.push(v);
    }
  }
  return result;
}

export type ColumnValuePair = [Column, any];
export type ColumnValuePairs = ColumnValuePair[];
export type RowValuePair = [Row, any];
export type RowValuePairs = RowValuePair[];
export type RowDict = { [key: string]: Row[] };

// wrapper around a row id (corresponds to an entity in an ECS)
export class Row {
  readonly _table: Table;
  readonly _id: string;

  // create
  constructor(table: Table, id: string) {
    this._table = table;
    this._id = id;
  }

  // check whether the row still exists in the table
  exists(): boolean { return this._table.rowExists(this); }

  // check whether a value exists
  has(c: Column): boolean { return this._table.valueExists(this, c); }

  // get value
  get(c: Column): any { return this._table.get(this, c); }

  // set value
  set(c: Column, value: any): void { this._table.set(this, c, value); }

  // remove a specific value from row, column
  deleteValue(c: Column): void { this._table.deleteValue(this, c); }

  // remove entire row
  deleteEntireRow(): void { this._table.deleteRow(this); }
}

// wrapper around a column id (corresponds to component data in an ECS)
export class Column {
  readonly _table: Table;
  readonly _id: string;

  // create
  constructor(table: Table, id: string) {
    this._table = table;
    this._id = id;
  }

  // check whether the column still exists in the table
  exists(): boolean { return this._table.columnExists(this); }

  // check whether a value exists
  has(r: Row): boolean { return this._table.valueExists(r, this); }

  // get component value for entity
  get(r: Row): any { return this._table.get(r, this); }

  // set component value for entity
  set(r: Row, value: any): void { this._table.set(r, this, value); }

  // remove a specific value from row, column
  deleteValue(r: Row): void { this._table.deleteValue(r, this); }

  // remove entire column
  deleteEntireColumn(): void { this._table.deleteColumn(this); }
}

// lists of rows
export class QueryResult {
  readonly _rows: string[];
  readonly _table: Table;

  // create
  constructor(table: Table, rows: string[]) {
    this._table = table;
    this._rows = rows;
  }

  // check that a column is truthy for all rows in the query
  _allColumn(column: Column): boolean {
    const rows = this._rows,
      numRows = this._rows.length,
      table = this._table,
      cId = column._id;
    for (let i = 0; i < numRows; i += 1) {
      if (!table._get(rows[i], cId)) {
        return false;
      }
    }
    return true;
  }

  // check that a function returns true for all rows in the query
  _allFn(fn: (r: Row) => boolean): boolean {
    const rows = this._rows,
      numRows = this._rows.length,
      table = this._table;
    for (let i = 0; i < numRows; i += 1) {
      if (!fn(new Row(table, rows[i]))) {
        return false;
      }
    }
    return true;
  }

  // check that a predicate is true for any row in the query
  all(pred: Column | ((r: Row) => boolean)): boolean {
    return (pred instanceof Column) ? this._allColumn(pred) : this._allFn(pred);
  }

  // check that a column is truthy for any row in the query
  _anyColumn(column: Column): boolean {
    const rows = this._rows,
      numRows = this._rows.length,
      table = this._table,
      cId = column._id;
    for (let i = 0; i < numRows; i += 1) {
      if (table._get(rows[i], cId)) {
        return true;
      }
    }
    return false;
  }

  // check that a function returns true for any row in the query
  _anyFn(fn: (r: Row) => boolean): boolean {
    const rows = this._rows,
      numRows = this._rows.length,
      table = this._table;
    for (let i = 0; i < numRows; i += 1) {
      if (fn(new Row(table, rows[i]))) {
        return true;
      }
    }
    return false;
  }

  // check that a predicate is true for at least one row in the query
  any(pred: Column | ((r: Row) => boolean)): boolean {
    return (pred instanceof Column) ? this._anyColumn(pred) : this._anyFn(pred);
  }

  // average for a column across a query
  _averageColumn(column: Column): number {
    const rows = this._rows,
      numRows = this._rows.length,
      table = this._table,
      cId = column._id;
    let sum = 0;
    for (let i = 0; i < numRows; i += 1) {
      sum += table._get(rows[i], cId);
    }
    return sum / numRows;
  }

  // average for a column across a query
  _averageFn(fn: (r: Row) => number): number {
    const rows = this._rows,
      numRows = this._rows.length,
      table = this._table;
    let sum = 0;
    for (let i = 0; i < numRows; i += 1) {
      sum += fn(new Row(table, rows[i]));
    }
    return sum / numRows;
  }

  // average for a value across a query
  average(pred: Column | ((r: Row) => number)): number {
    return (pred instanceof Column) ? this._averageColumn(pred) : this._averageFn(pred);
  }

  // rows in this query, followed by rows in other. may cause duplicates
  concat(other: QueryResult): QueryResult {
    if (this._table !== other._table) { throw new Error("trying to concat results from different tables!"); }
    return new QueryResult(this._table, this._rows.concat(other._rows));
  }

  // whether the row is in the result
  contains(r: Row): boolean {
    return (-1 !== this._rows.indexOf(r._id));
  }

  // count of rows in the result
  count(): number {
    return this._rows.length;
  }

  // filter out duplicate rows (produces a new QueryResult)
  // compares the results of calling fn for each row, emits
  // row that produce a result it hasn't seen yet
  _distinctFn(fn: (r: Row) => any): QueryResult {
    const rows = this._rows,
      numRows = this._rows.length,
      table = this._table,
      fnResults: any[] = [],
      resultRows: string[] = [];
    for (let i = 0; i < numRows; i += 1) {
      const rId = rows[i],
        result = fn(new Row(table, rId));
      if (-1 === fnResults.indexOf(result)) {
        fnResults.push(result);
        resultRows.push(rId);
      }
    }
    return new QueryResult(table, resultRows);
  }

  // filter out duplicate rows (produces a new QueryResult)
  _distinct(): QueryResult {
    const rows = this._rows,
      numRows = this._rows.length,
      table = this._table,
      foundRowIds: { [key: string]: null } = {},
      resultRows: string[] = [];
    for (let i = 0; i < numRows; i += 1) {
      const rId = rows[i];
      if (!(rId in foundRowIds)) {
        foundRowIds[rId] = null;
        resultRows.push(rId);
      }
    }
    return new QueryResult(table, resultRows);
  }

  // filter out duplicate rows (produces a new QueryResult)
  // considers only row if argument is null or undefined
  // compares function results if a function is provided
  distinct(fn: undefined | ((r: Row) => any)) {
    if (!!fn) {
      return this._distinctFn(fn);
    }
    return this._distinct();
  }

  // query containing rows in this but not in other
  except(other: QueryResult): QueryResult {
    if (this._table !== other._table) { throw new Error("trying to concat results from different tables!"); }
    return new QueryResult(this._table, arrayExcept(this._rows, other._rows));
  }

  // sort the rows into string-keyed buckets
  groupBy(fn: ((r: Row) => string)): RowDict {
    const result: RowDict = {},
      rows = this._rows,
      numRows = this._rows.length,
      table = this._table;
    for (let i = 0; i < numRows; i += 1) {
      const rId = rows[i],
        row = new Row(table, rId),
        key = fn(row);
      if (!(key in result)) {
        result[key] = [];
      }
      result[key].push(row);
    }
    return result;
  }

  // call a function on every row in the query
  forEach(fn: (r: Row) => void): QueryResult {
    const rows = this._rows,
      numRows = this._rows.length,
      table = this._table;
    for (let i = 0; i < numRows; i += 1) {
      fn(new Row(table, rows[i]));
    }
    return this;
  }

  // method ideas:
  //      intersect(query2): Query - query containing rows that are in both queries
  //      join(query2, fn1, fn2): Query - query containing rows where fn1(row from this) === fn2(row from query2)
  //      max(Column | fn: (Row)=>number): number
  //      map(Column | fn: (Row)=>boolean)=>any): any[]
  //      min(Column | fn: (Row)=>number)): number
  //      orderBy(Column, optional fn to run the value through): Query - sort ascending
  //      orderByDesc(Column, optional fn to run the value through): Query - sort descending
  //      prepend(query2): Query - rows in query2, followed by rows in this. may cause duplicates
  //      shuffle(): this - randomize the order of the query
  //      skip
  //      skipWhile
  //      sum(Column | fn: (Row)=>number): number
  //      take
  //      takeWhile
  //      asArray
  //      union
  //      where
  //      zip
  //      ES6 iterators
}

// the big database
export default class Table {
  // used to make unique ids
  _counter = 0

  // re-usable ids
  readonly _freeIds: string[] = [];

  // all row ids
  readonly _allRows: { [key: string]: null } = {};

  // table data
  readonly _data: { [key: string]: { [key: string]: any } } = {};

  // create
  constructor() {
    this.reset();
  }

  // reserve an id
  _takeId(): string {
    if (this._freeIds.length > 0) {
      return this._freeIds.pop() || "";
    }
    if (this._counter >= Number.MAX_SAFE_INTEGER) {
      throw new Error("ran out of ids");
    }
    return "`" + (this._counter++).toString(36);
  }

  // create a new row
  addRow(): Row {
    const id = this._takeId();
    this._allRows[id] = null;
    return new Row(this, id);
  }

  // create and initialize a new row
  addRowWithData(rowData: ColumnValuePairs): Row {
    const r: Row = this.addRow(),
      rId = r._id;
    for (let [column, value] of rowData) {
      this._data[rId][column._id] = value;
    }
    return r;
  }

  // create a new column
  addColumn(): Column {
    const id = this._takeId();
    this._data[id] = {};
    return new Column(this, id);
  }

  // create and initialize a new column
  addColumnWithData(columnData: RowValuePairs): Column {
    const c: Column = this.addColumn(),
      cId = c._id;
    for (let [row, value] of columnData) {
      this._data[row._id][cId] = value;
    }
    return c;
  }

  // remove an entire row
  deleteRow(r: Row): void {
    const data = this._data,
      rId = r._id;
    for (let cId in data) {
      const cData = data[cId];
      delete cData[rId];
    }
    this._freeIds.push(rId);
  }

  // remove an entire column
  deleteColumn(c: Column): void {
    const data = this._data,
      cId = c._id;
    delete data[cId];
  }

  // does the given row still exist in the table?
  rowExists(r: Row): boolean {
    return (r._id in this._allRows);
  }

  // does the given column still exist in the table?
  columnExists(c: Column): boolean {
    return (c._id in this._data);
  }

  // does a value exist for the given row/column
  valueExists(r: Row, c: Column): boolean {
    const rId = r._id,
      cId = c._id,
      data = this._data;
    return (cId in data) && (rId in (data[cId]));
  }

  // get a value from the table
  _get(rId: string, cId: string): any {
    return this._data[cId][rId];
  }

  // get a value from the table
  get(r: Row, c: Column): any {
    return this._data[c._id][r._id];
  }

  // get values from the table for a given row
  get2(r: Row, c0: Column, c1: Column): [any, any] {
    const data = this._data;
    return [
      data[c0._id][r._id],
      data[c1._id][r._id]
    ];
  }

  // get values from the table for a given row
  get3(r: Row, c0: Column, c1: Column, c2: Column): [any, any, any] {
    const data = this._data;
    return [
      data[c0._id][r._id],
      data[c1._id][r._id],
      data[c2._id][r._id]
    ];
  }

  // get values from the table for a given row
  get4(r: Row, c0: Column, c1: Column, c2: Column, c3: Column): [any, any, any, any] {
    const data = this._data;
    return [
      data[c0._id][r._id],
      data[c1._id][r._id],
      data[c2._id][r._id],
      data[c3._id][r._id]
    ];
  }

  // get values from the table for a given row
  getN(r: Row, columns: Column[]): any[] {
    const data = this._data,
      n = columns.length,
      result = [];
    for (let i = 0; i < n; i += 1) {
      result.push(data[columns[i]._id][r._id]);
    }
    return result;
  }

  // set a value in the table
  set(r: Row, c: Column, value: any): void {
    this._data[c._id][r._id] = value;
  }

  // remove a value from the table
  deleteValue(r: Row, c: Column): void {
    const cData = this._data[c._id];
    delete cData[r._id];
  }

  // clear the entire table
  reset(): void {
    this._counter = 0;
    this._freeIds.length = 0;
    clearObject(this._allRows);
    clearObject(this._data);
  }

  // get rows based on columns they have values in
  select(...columns: Column[]): QueryResult {
    const allRows: string[] = Object.keys(this._allRows),
      numRows = allRows.length,
      numColumns = columns.length,
      data = this._data;
    if (columns.length === 0) {
      return new QueryResult(this, allRows);
    }
    const filteredRows = [];
    for (let i = 0; i < numRows; i += 1) {
      const rId = allRows[i];
      let inAll = true;
      for (let j = 0; j < numColumns; j += 1) {
        const cData = data[columns[j]._id];
        if (!(rId in cData)) {
          inAll = false;
          break;
        }
      }
      if (inAll) {
        filteredRows.push(rId);
      }
    }
    return new QueryResult(this, filteredRows);
  }
}
