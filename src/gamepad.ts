export type EventListener = (arg0: KeyboardEvent) => void;

let prevX = 0,
  prevY = 0,
  prevBtn0 = 0,
  prevBtn2 = 0,
  listeners: EventListener[] = [],
  round = Math.round,
  ticksSinceChange = 0,
  repeatEvery = 10,
  pollIntervalTime = 50,
  pollIntervalId: null | number = null;
function tellListeners(key: string) {
  const e = makeKeyEvent(key),
    listCopy = listeners.concat(),
    n = listCopy.length;
  for (var i = 0; i < n; i += 1) {
    listCopy[i](e);
  }
}
function makeKeyEvent(key: string): KeyboardEvent {
  return new KeyboardEvent("keydown", { key });
}
function poll() {
  var gamepads = navigator.getGamepads ? navigator.getGamepads() : [];
  if (!gamepads) {
    return;
  }
  if (gamepads.length < 1) {
    return;
  }
  var gp = gamepads[0];
  if (gp === null) {
    return;
  }
  var x = round(gp.axes[0]),
    y = round(gp.axes[1]),
    btn0 = round(gp.buttons[0].value),
    btn2 = round(gp.buttons[2].value);

  ticksSinceChange += 1;

  // pretend it recentered to cause key repeat-like behavior
  if (ticksSinceChange >= repeatEvery) {
    prevX = 0;
    prevY = 0;
    ticksSinceChange = 0;
  }

  if (prevX !== x) {
    prevX = x;
    prevY = y;
    ticksSinceChange = 0;
    if (x < 0) {
      tellListeners("Left");
    } else if (x > 0) {
      tellListeners("Right");
    }
  } else if (prevY !== y) {
    prevX = x;
    prevY = y;
    ticksSinceChange = 0;
    if (y < 0) {
      tellListeners("Up");
    } else if (y > 0) {
      tellListeners("Down");
    }
  } else if (prevBtn0 !== btn0) {
    prevBtn0 = btn0;
    ticksSinceChange = 0;
    if (btn0 > 0) {
      tellListeners("B");
    }
  } else if (prevBtn2 !== btn2) {
    prevBtn2 = btn2;
    ticksSinceChange = 0;
    if (btn2 > 0) {
      tellListeners("R");
    }
  }
}

export function addListener(cb: EventListener) {
  listeners.push(cb);
  poll();
  if (pollIntervalId === null) {
    pollIntervalId = setInterval(poll, pollIntervalTime);
  }
}
export function removeListener(cb: EventListener) {
  var n = listeners.length;
  while (n-- > 0) {
    if (listeners[n] === cb) {
      listeners.splice(n, 1);
    }
  }
  if (pollIntervalId !== null) {
    clearInterval(pollIntervalId);
    pollIntervalId = null;
  }
}

setInterval(poll, pollIntervalTime);

export default {
  addListener: addListener,
  removeListener: removeListener,
};
