import * as ROT from "rot-js/lib/index"
import { CreateCallback } from "rot-js/lib/map/map"
import * as settings from "./settings"

// shortcuts
const
  EPSILON = Number.EPSILON,
  floor = Math.floor,
  max = Math.max,
  min = Math.min;

// first pass of map generation before adding features and things
export function generateBaseMap(w: number, h: number, level: number, cb: CreateCallback) {
  // clear out any old level
  for (let x=0;x<w;x+=1) {
    for (let y=0;y<h;y+=1) {
      cb(x, y, 1);
    }
  }
  const type = ROT.RNG.getWeightedValue(settings.mapAlgoWeights);
  if ("digger" === type) {
    const
      roomWidth: [number, number] = [floor(settings.roomWidthMinFn(level) + EPSILON), floor(settings.roomWidthMaxFn(level) + EPSILON)],
      roomHeight: [number, number] = [floor(settings.roomHeightMinFn(level) + EPSILON), floor(settings.roomHeightMaxFn(level) + EPSILON)],
      corridorLength: [number, number] = [floor(settings.corridorLengthMinFn(level) + EPSILON), floor(settings.corridorLengthMaxFn(level) + EPSILON)],
      dugPercentage: number = settings.digPctFn(level),
      levelOptions = {
        roomWidth,
        roomHeight,
        corridorLength,
        dugPercentage
      },
      digger = new ROT.Map.Digger(w, h, levelOptions);
    digger.create(cb);
  } else if ("eller" === type) {
    const
      maxMargin = floor((min(w, h) - 6) / 2),
      margin = floor(max(0, maxMargin + 1 - level)),
      fudgedCb = function(x: number, y: number, value: number) {
        cb(x + margin, y + margin, value);
      },
      eller = new ROT.Map.EllerMaze(w - 2 * margin, h - 2 * margin);
    eller.create(fudgedCb);
  } else if ("ellerBig" === type) {
    const
      maxMargin = floor((min(w, h) - 9) / 2),
      margin = floor(max(0, maxMargin + 1 - level)),
      ellerW = floor(((w - 2 * margin) - 3) * 2 / 3) + 3,
      ellerH = floor(((h - 2 * margin) - 3) * 2 / 3) + 3,
      fudgedCb = function(x: number, y: number, value: number) {
        const cols = ((x !== 0) && (x !== ellerW-1) && (x%2===0)) ? 2 : 1,
          rows = ((y !== 0) && (y !== ellerH-1) && (y%2===0)) ? 2 : 1;
        for (let i=0;i<rows;i+=1) {
          const destY = margin + i + max(0, floor((y-1) * 3 / 2) + 1);
          for (let j=0;j<cols;j+=1) {
            const destX = margin + j + max(0, floor((x-1) * 3 / 2) + 1);
            cb(destX, destY, value);
          }
        }
      },
      eller = new ROT.Map.EllerMaze(ellerW, ellerH);
    eller.create(fudgedCb);
  }
}
