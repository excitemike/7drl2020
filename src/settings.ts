
// shortcuts
const
  EPSILON = Number.EPSILON,
  abs = Math.abs,
  ln = Math.log;


export const curtainColor = "#666";
export const levelChangeColor = "#666";
export const mapAlgoWeights = {
  "eller": 1,
  "ellerBig": 1,
  "digger": 0
};
export const bombRadius = 2.75;

// LEVEL GEN SETTINGS

// create the function to progress a value logarithmically
function makeLogSettingFn(x0: number, y0: number, x1: number, y1: number) {
  var lnx1 = ln(x1),
    lnx0 = ln(x0),
    deltaln = lnx1 - lnx0,
    A = (abs(deltaln) < EPSILON) ? 0 : (y1 - y0) / deltaln,
    C = (abs(deltaln) < EPSILON) ? y0 : (y0 * lnx1 - y1 * lnx0) / deltaln;
  return function(n: number) { return A * ln(n) + C; };
}

// create the function to progress a value linearly
function makeLinearSettingFn(x0: number, y0: number, x1: number, y1: number) {
  return function(n: number) { return y0 + (n - x0) * (y1 - y0) / (x1 - x0); };
}

const highLevel = 20;
export const tailLengthFn = makeLogSettingFn(1, 3, highLevel, 4)
export const corridorLengthMinFn = makeLogSettingFn(1, 2, highLevel, 3);
export const corridorLengthMaxFn = makeLogSettingFn(1, 4, highLevel, 6);
export const digPctFn = makeLogSettingFn(1, 0.1, highLevel, 0.5);
export const roomWidthMinFn = makeLogSettingFn(1, 10, highLevel, 4);
export const roomWidthMaxFn = makeLogSettingFn(1, 12, highLevel, 8);
export const roomHeightMinFn = makeLogSettingFn(1, 4, highLevel, 2);
export const roomHeightMaxFn = makeLogSettingFn(1, 6, highLevel, 4);
export const numNotesMinFn = makeLogSettingFn(1, 3, highLevel, 5);
export const numNotesMaxFn = makeLogSettingFn(1, 3, highLevel, 8);
export const numEnemiesMinFn = makeLinearSettingFn(1, 1, highLevel, 4);
export const numEnemiesMaxFn = makeLinearSettingFn(1, 1, highLevel, 6);
export const enemyWeightW = makeLogSettingFn(1, 1, highLevel, 2);
export const enemyWeightS = makeLogSettingFn(1, 0, highLevel, 4);
export const enemyWeightA = makeLogSettingFn(1, 0, highLevel, 5);
export const enemyWeightR = makeLogSettingFn(1, 0, highLevel, 6);
export const numBombsMinFn = function(n: number) { return ((n > 1) && (n < 6)) ? 1 : 0; };
export const numBombsMaxFn = function(n: number) { return (n === 1) ? 0 : 1; };
