import * as ROT from "rot-js/lib/index"
import { CreateCallback } from "rot-js/lib/map/map"
import { PassableCallback } from "rot-js/lib/path/path"

type Stamp = {
  w: number,
  h: number,
  stamp: string
};
const randRange = ROT.RNG.getUniformInt.bind(ROT.RNG),
  randFromArray = ROT.RNG.getItem.bind(ROT.RNG),
  stamps = [
    { w: 5, h: 5, stamp: "?......##..##....#..?...?" }, // "f" pentomino
    { w: 7, h: 3, stamp: "........#####........" }, // 5-wall
    { w: 6, h: 4, stamp: "...???.#.....####......." }, // "L" pentomino
    { w: 6, h: 4, stamp: "....??.##.....###.?....." }, // "n" pentomino
    { w: 4, h: 5, stamp: ".....##..##..#.....?" }, // "p" pentomino
    { w: 5, h: 5, stamp: "?...??.#.?..#...###......" }, // "t" pentomino
    //disallowed because it makes a deadend {w: 5, h: 4, stamp: "......#.#..###......"}, // "u" pentomino
    { w: 5, h: 5, stamp: "??...??.#....#..###......" }, // "v" pentomino
    { w: 5, h: 5, stamp: "??...?..#...##..##......?" }, // "w" pentomino
    { w: 5, h: 5, stamp: "?...?..#...###...#..?...?" }, // "x" pentomino
    { w: 4, h: 6, stamp: "?.....#..##...#.?.#.?..." }, // "y" pentomino
    { w: 5, h: 5, stamp: "....?.##.?..#..?.##.?...." }, // "z" pentomino

    { w: 5, h: 4, stamp: "......###.?.#.??...?" }, // T tetromino
    { w: 5, h: 4, stamp: "......###..#......??" }, // L tetromino
    { w: 5, h: 4, stamp: "....?.##....##.?...." }, // S tetromino
    { w: 4, h: 4, stamp: ".....##..##....." }, // square tetromino
    { w: 6, h: 3, stamp: ".......####......." }, // line tetromino
    { w: 4, h: 4, stamp: "...?.#...##....." }, // L triomino
    { w: 5, h: 3, stamp: "......###......" }, // 1x3
    { w: 4, h: 3, stamp: ".....##....." }, // 1x2
    { w: 3, h: 3, stamp: "....#...." }, // pillar

    // begin hexominoes
    { w: 3, h: 8, stamp:
      "..." +
      ".#." +
      ".#." +
      ".#." +
      ".#." +
      ".#." +
      ".#." +
      "..."},
    { w: 4, h: 7, stamp:
      "...." +
      ".##." +
      ".#.." +
      ".#.?" +
      ".#.?" +
      ".#.?" +
      "...?"},
    { w: 4, h: 7, stamp:
      "...?" +
      ".#.." +
      ".##." +
      ".#.." +
      ".#.?" +
      ".#.?" +
      "...?"},
    { w: 4, h: 7, stamp:
      "...?" +
      ".#.?" +
      ".#.." +
      ".##." +
      ".#.." +
      ".#.?" +
      "...?"},
    { w: 4, h: 7, stamp:
      "?..." +
      "..#." +
      ".##." +
      ".#.." +
      ".#.?" +
      ".#.?" +
      "...?"},
    { w: 4, h: 6, stamp:
      "...." +
      ".##." +
      ".##." +
      ".#.." +
      ".#.?" +
      "...?"},
    // disallowed due to dead-end:
    // ....
    // .##.
    // .#..
    // .##.
    // .#..
    // ....
    { w: 4, h: 6, stamp:
      "...." +
      ".##." +
      ".#.." +
      ".#.." +
      ".##." +
      "...."},
    { w: 4, h: 6, stamp:
      "...?" +
      ".#.." +
      ".##." +
      ".##." +
      ".#.." +
      "...?"},
    { w: 5, h: 6, stamp:
      "....." +
      ".###." +
      ".#..." +
      ".#.??" +
      ".#.??" +
      "...??"},
    { w: 5, h: 6, stamp:
      "....." +
      ".#..." +
      ".###." +
      ".#..." +
      ".#.??" +
      "...??"},
    { w: 5, h: 6, stamp:
      "....." +
      ".###." +
      "..#.." +
      "?.#.?" +
      "?.#.?" +
      "?...?"},
    { w: 5, h: 6, stamp:
      "?...." +
      "..##." +
      ".##.." +
      "..#.?" +
      "?.#.?" +
      "?...?"},
    { w: 5, h: 6, stamp:
      "?...." +
      "?.##." +
      "..#.." +
      ".##.?" +
      "..#.?" +
      "?...?"},
    { w: 5, h: 6, stamp:
      "?...." +
      "?.##." +
      "?.#.." +
      "..#.?" +
      ".##.?" +
      "....?"},
    { w: 5, h: 6, stamp:
      "?...?" +
      "?.#.." +
      "..##." +
      ".##.." +
      "..#.?" +
      "?...?"},
    { w: 5, h: 6, stamp:
      "?...?" +
      "..#.." +
      ".###." +
      "..#.." +
      "?.#.?" +
      "?...?"},
    { w: 5, h: 6, stamp:
      "?...?" +
      "..#.." +
      ".###." +
      ".#..." +
      ".#.??" +
      "...??"},
    // disallowed due to dead-end:
    // ?...
    // ..#.
    // .##.
    // .#..
    // .##.
    // ....
    { w: 4, h: 7, stamp:
      "?..." +
      "..#." +
      "..#." +
      ".##." +
      ".#.." +
      ".#.?" +
      "...?"},
    { w: 4, h: 6, stamp:
      "?..." +
      "..#." +
      ".##." +
      ".##." +
      ".#.." +
      "...?"},
    { w: 5, h: 4, stamp: "......###..###......" }, // 2x3 block
    { w: 5, h: 6, stamp:
      "??..." +
      "...#." +
      ".###." +
      "..#.." +
      "?.#.?" +
      "?...?"},
    { w: 5, h: 5, stamp:
      "....." +
      ".###." +
      "..##." +
      "?.#.." +
      "?...?"},
    { w: 5, h: 6, stamp:
      "??..." +
      "?..#." +
      "..##." +
      ".##.." +
      "..#.?" +
      "?...?"},
    { w: 5, h: 6, stamp:
      "??..." +
      "...#." +
      ".###." +
      ".#..." +
      ".#.??" +
      "...??"},
    { w: 5, h: 6, stamp:
      "?...." +
      "..##." +
      ".##.." +
      ".#..?" +
      ".#.??" +
      "...??"},
    // disallowed due to dead-end
    //  .....
    //  .###.
    //  .#.#.
    //  .#...
    //  ...??
    // disallowed due to dead-end
    //  .....
    //  .#.#.
    //  .###.
    //  .#...
    //  ...??
    // disallowed due to dead-end
    //  .....
    //  .#.#.
    //  .###.
    //  ..#..
    //  ?...?
    { w: 5, h: 6, stamp:
      "?...." +
      "?.##." +
      "..#.." +
      ".##.?" +
      ".#..?" +
      "...??"},
    { w: 5, h: 5, stamp:
      "...??" +
      ".#..?" +
      ".##.." +
      ".###." +
      "....."},
    { w: 5, h: 5, stamp:
      "?...?" +
      "..#.." +
      ".###." +
      ".##.." +
      "....?"},
    { w: 5, h: 5, stamp:
      "??..." +
      "...#." +
      ".###." +
      ".##.." +
      "....?"},
    { w: 5, h: 6, stamp:
      "??..." +
      "?..#." +
      "..##." +
      ".##.." +
      ".#..?" +
      "...??"},
    // end hexominoes

    // pacman inspired
    { w: 7, h: 5, stamp:
      "...?..." +
      ".#.#.#." +
      ".#...#." +
      ".#####." +
      "......."}, // U2
    { w: 8, h: 5, stamp:
      "...??..." +
      ".#.##.#." +
      ".#....#." +
      ".##..##." +
      "........"}, // Y-shaped path (wide)
    { w: 7, h: 5, stamp:
      "...?..." +
      ".#.#.#." +
      ".#...#." +
      ".##.##." +
      "......."}, // Y-shaped path (thin)
    { w: 9, h: 5, stamp:
      "...???..." +
      ".#.###.#." +
      ".#..#..#." +
      ".##...##." +
      "....?...."}, // very wide Y shaped path
    { w: 9, h: 5, stamp:
      "...???..." +
      ".#.###.#." +
      ".#..#..#." +
      ".##.#.##." +
      "....?...."}, // mirrored zigzag paths

    // rooms
    { w: 2, h: 2, stamp: "...." },
    { w: 3, h: 2, stamp: "......" },
    { w: 3, h: 3, stamp: "........." },
    { w: 4, h: 3, stamp: "............" },
    { w: 4, h: 4, stamp: "................" },
    { w: 5, h: 4, stamp: "...................." },
    { w: 5, h: 5, stamp: "........................." },
    { w: 6, h: 6, stamp:
      "......" +
      ".##.#." +
      ".#..#." +
      ".#..#." +
      ".#.##." +
      "......"},
    { w: 6, h: 6, stamp:
      "......" +
      ".#.##." +
      ".#..#." +
      ".#..#." +
      ".#.##." +
      "......"},
    { w: 6, h: 6, stamp:
      "......" +
      ".####." +
      ".#...." +
      ".#..#." +
      ".#.##." +
      "......"},
    { w: 6, h: 6, stamp:
      "......" +
      ".####." +
      ".#..#." +
      ".#...." +
      ".#.##." +
      "......"},
    { w: 8, h: 8, stamp:
      "........" +
      ".####.#." +
      ".#....#." +
      ".#....#." +
      ".#....#." +
      ".#....#." +
      ".#.####." +
      "........"},
    { w: 8, h: 8, stamp:
      "........" +
      ".###.##." +
      ".#....#." +
      ".#....#." +
      ".#....#." +
      ".#....#." +
      ".#.####." +
      "........"},
    { w: 8, h: 8, stamp:
      "........" +
      ".##.###." +
      ".#....#." +
      ".#....#." +
      ".#....#." +
      ".#....#." +
      ".#.####." +
      "........"},
    { w: 8, h: 8, stamp:
      "........" +
      ".#.####." +
      ".#....#." +
      ".#....#." +
      ".#....#." +
      ".#....#." +
      ".#.####." +
      "........"},
    { w: 8, h: 8, stamp:
      "........" +
      ".###.##." +
      ".#....#." +
      ".#....#." +
      ".#....#." +
      ".#....#." +
      ".##.###." +
      "........"},
    { w: 8, h: 8, stamp:
      "........" +
      ".##.###." +
      ".#....#." +
      ".#....#." +
      ".#....#." +
      ".#....#." +
      ".##.###." +
      "........"},
    { w: 8, h: 8, stamp:
      "........" +
      ".######." +
      ".#......" +
      ".#....#." +
      ".#....#." +
      ".#....#." +
      ".#.####." +
      "........"},
    { w: 8, h: 8, stamp:
      "........" +
      ".######." +
      ".#....#." +
      ".#......" +
      ".#....#." +
      ".#....#." +
      ".#.####." +
      "........"},
    { w: 8, h: 8, stamp:
      "........" +
      ".######." +
      ".#....#." +
      ".#....#." +
      ".#......" +
      ".#....#." +
      ".#.####." +
      "........"},
    { w: 8, h: 8, stamp:
      "........" +
      ".######." +
      ".#....#." +
      ".#....#." +
      ".#....#." +
      ".#......" +
      ".#.####." +
      "........"},
    { w: 8, h: 8, stamp:
      "........" +
      ".######." +
      ".#....#." +
      ".#......" +
      ".#....#." +
      ".#....#." +
      ".##.###." +
      "........"},
    { w: 8, h: 8, stamp:
      "........" +
      ".######." +
      ".#....#." +
      ".#....#." +
      ".#......" +
      ".#....#." +
      ".##.###." +
      "........"},
    { w: 11, h: 11, stamp:
      "..........." +
      ".##.###.##." +
      ".#.......#." +
      "..........." +
      ".#.......#." +
      ".#.......#." +
      ".#.......#." +
      "..........." +
      ".#.......#." +
      ".##.###.##." +
      "..........."},
    { w: 7, h: 7, stamp:
      "......." +
      ".##.##." +
      ".#...#." +
      "......." +
      ".#...#." +
      ".##.##." +
      "......."},
  ];

function rotateStampCw(stampObj: Stamp) {
  var output = { w: stampObj.h, h: stampObj.w, stamp: "" },
    dstX,
    dstY,
    srcI;
  for (dstY = 0; dstY < output.h; dstY += 1) {
    for (dstX = 0; dstX < output.w; dstX += 1) {
      srcI = (output.w - dstX - 1) * output.h + dstY;
      output.stamp += stampObj.stamp[srcI];
    }
  }
  return output;
}
function rotateStamp180(stampObj: Stamp) {
  var output = { w: stampObj.w, h: stampObj.h, stamp: "" },
    dstX,
    dstY,
    srcI;
  for (dstY = 0; dstY < output.h; dstY += 1) {
    for (dstX = 0; dstX < output.w; dstX += 1) {
      srcI = (output.h - dstY) * output.w - 1 - dstX;
      output.stamp += stampObj.stamp[srcI];
    }
  }
  return output;
}
function rotateStampCcw(stampObj: Stamp) {
  var output = { w: stampObj.h, h: stampObj.w, stamp: "" },
    dstX,
    dstY,
    srcI;
  for (dstY = 0; dstY < output.h; dstY += 1) {
    for (dstX = 0; dstX < output.w; dstX += 1) {
      srcI = (dstX + 1) * output.h - 1 - dstY;
      output.stamp += stampObj.stamp[srcI];
    }
  }
  return output;
}
function reflectStampH(stampObj: Stamp) {
  var output = { w: stampObj.w, h: stampObj.h, stamp: "" },
    dstX,
    dstY,
    srcI;
  for (dstY = 0; dstY < output.h; dstY += 1) {
    for (dstX = 0; dstX < output.w; dstX += 1) {
      srcI = (dstY + 1) * output.w - 1 - dstX;
      output.stamp += stampObj.stamp[srcI];
    }
  }
  return output;
}
function reflectStampDiagonally1(stampObj: Stamp) { return rotateStampCw(reflectStampH(stampObj)); }
function reflectStampV(stampObj: Stamp) {
  var output = { w: stampObj.w, h: stampObj.h, stamp: "" },
    dstX,
    dstY,
    srcI;
  for (dstY = 0; dstY < output.h; dstY += 1) {
    for (dstX = 0; dstX < output.w; dstX += 1) {
      srcI = (output.h - 1 - dstY) * output.w + dstX;
      output.stamp += stampObj.stamp[srcI];
    }
  }
  return output;
}
function reflectStampDiagonally2(stampObj: Stamp) { return rotateStampCcw(reflectStampH(stampObj)); }
function identity(stampObj: Stamp) { return stampObj; }

let stampTransforms = [
  identity,
  rotateStampCw,
  rotateStamp180,
  rotateStampCcw,
  reflectStampH,
  reflectStampDiagonally1,
  reflectStampV,
  reflectStampDiagonally2
];

function randomStampTransform(stampObj: Stamp) {
  var i = randRange(0, stampTransforms.length - 1);
  return stampTransforms[i](stampObj);
}
function randomStamp() {
  var i = randRange(0, stamps.length - 1);
  return randomStampTransform(stamps[i]);
}
function applyStamp(x: number, y: number, stampObj: Stamp, digCallback: CreateCallback) {
  var x2,
    y2,
    c;
  for (y2 = 0; y2 < stampObj.h; y2 += 1) {
    for (x2 = 0; x2 < stampObj.w; x2 += 1) {
      c = stampObj.stamp[y2 * stampObj.w + x2];
      if (c === ".") {
        digCallback(x + x2, y + y2, 0);
      } else if (c === "#") {
        digCallback(x + x2, y + y2, 1);
      }
    }
  }
}
//
function applyRandomStamp(minX: number, minY: number, maxX: number, maxY: number, passableCb: PassableCallback, digCallback: CreateCallback) {
  var tries = 100,
    x,
    y,
    x2,
    y2,
    stampObj = randomStamp();
  while (tries > 0) {
    tries -= 1;
    x = ROT.RNG.getUniformInt(minX, maxX - stampObj.w);
    y = ROT.RNG.getUniformInt(minY, maxY - stampObj.h);
    for (y2 = 0; y2 < stampObj.h; y2 += 1) {
      for (x2 = 0; x2 < stampObj.w; x2 += 1) {
        if ((stampObj.stamp[y2 * stampObj.w + x2] === ".") && passableCb(x + x2, y + y2)) {
          applyStamp(x, y, stampObj, digCallback);
          return;
        }
      }
    }
  }
}
//
function applyRandomStampAt(x: number, y: number, minX: number, minY: number, maxX: number, maxY: number, digCallback: CreateCallback) {
  var tries = 100,
    x2,
    y2,
    stampObj = randomStamp(),
    options = [];
  while (tries > 0) {
    tries -= 1;
    for (y2 = 0; y2 < stampObj.h; y2 += 1) {
      for (x2 = 0; x2 < stampObj.w; x2 += 1) {
        if ((stampObj.stamp[y2 * stampObj.w + x2] === ".") &&
            (minX <= x-x2) &&
            (minY <= y-y2) &&
            (maxX >= x-x2+stampObj.w) &&
            (maxY >= y-y2+stampObj.h)) {
          options.push([x2,y2]);
        }
      }
    }
    if (options.length > 0) {
      break;
    }
  }
  if (options.length > 0) {
    [x2, y2] = randFromArray(options);
    applyStamp(x-x2, y-y2, stampObj, digCallback);
    return true;
  }
  return false;
}

export default {
  applyRandomStamp: applyRandomStamp,
  applyRandomStampAt: applyRandomStampAt
};
